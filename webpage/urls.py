from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('contactanos', views.contactanos, name='contactanos'),

    path('games', views.games, name='games'),
    path('games/pong', views.gamePong, name='gamePong'),
    path('games/arkanoid', views.gameArkanoid, name='gameArkanoid'),
    path('games/space_indavers', views.gameSpaceInvaders, name='gameSpaceInvaders'),

    path('scores', views.scores, name='scores'),
    path('scores/pong', views.scorePong, name='scorePong'),
    path('scores/arkanoid', views.scoreArkanoid, name='scoreArkanoid'),
    path('scores/space_indavers', views.scoreSpaceInvaders, name='scoreSpaceInvaders'),

    path('unity/arkanoid', views.arkanoid, name='arkanoid')
]