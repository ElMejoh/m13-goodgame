from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.

from webpage.models import Games, Puntuaciones

def index(request):
    pong = Games.objects.get(pk=1)
    arkanoid = Games.objects.get(pk=2)
    space_invader = Games.objects.get(pk=3)

    games = Games.objects.all()
    puntuaciones = Puntuaciones.objects.all()

    return render(request, 'webpage/index.html', {
        'pong': pong,
        'arkanoid': arkanoid,
        'space_invader': space_invader,
        'games' : games,
        'puntuaciones' : puntuaciones
    })

def contactanos(request):
    pong = Games.objects.get(pk=1)
    arkanoid = Games.objects.get(pk=2)
    space_invader = Games.objects.get(pk=3)

    return render(request, 'webpage/contactanos.html', {
        'pong': pong,
        'arkanoid': arkanoid,
        'space_invader': space_invader
    })

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

# -- GAMES -- #
def games(request):
    games = Games.objects.all()

    pong = Games.objects.get(pk=1)
    arkanoid = Games.objects.get(pk=2)
    space_invader = Games.objects.get(pk=3)

    return render(request, 'webpage/Games/games.html', {
        'pong': pong,
        'arkanoid': arkanoid,
        'space_invader': space_invader,
        'games' : games,

    })


def gamePong(request):
    pong = Games.objects.get(pk=1)
    arkanoid = Games.objects.get(pk=2)
    space_invader = Games.objects.get(pk=3)

    return render(request, 'webpage/Games/game_Pong.html', {
        'pong': pong,
        'arkanoid': arkanoid,
        'space_invader': space_invader
    })

def gameArkanoid(request):
    pong = Games.objects.get(pk=1)
    arkanoid = Games.objects.get(pk=2)
    space_invader = Games.objects.get(pk=3)

    return render(request, 'webpage/Games/game_Arkanoid.html', {
        'pong': pong,
        'arkanoid': arkanoid,
        'space_invader': space_invader
    })

def gameSpaceInvaders(request):
    pong = Games.objects.get(pk=1)
    arkanoid = Games.objects.get(pk=2)
    space_invader = Games.objects.get(pk=3)

    return render(request, 'webpage/Games/game_SpaceInvaders.html', {
        'pong': pong,
        'arkanoid': arkanoid,
        'space_invader': space_invader
    })

# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ #

# -- SCORES -- #

def scores(request):
    scores = Puntuaciones.objects.all()

    pong = Games.objects.get(pk=1)
    arkanoid = Games.objects.get(pk=2)
    space_invader = Games.objects.get(pk=3)

    return render(request, 'webpage/Scores/scores.html', {
        'scores' : scores,
        'pong': pong,
        'arkanoid': arkanoid,
        'space_invader': space_invader
    })

def scorePong(request):
    pong = Games.objects.get(pk=1)
    arkanoid = Games.objects.get(pk=2)
    space_invader = Games.objects.get(pk=3)

    scores = Puntuaciones.objects.all().filter(gameFK=1).order_by('-puntuacion')
    x = 1

    return render(request, 'webpage/Scores/score_Pong.html', {
        'pong': pong,
        'arkanoid': arkanoid,
        'space_invader': space_invader,

        'scores' : scores,
        'x' : x
    })

def scoreArkanoid(request):
    pong = Games.objects.get(pk=1)
    arkanoid = Games.objects.get(pk=2)
    space_invader = Games.objects.get(pk=3)

    scores = Puntuaciones.objects.all().filter(gameFK=2).order_by('-puntuacion')

    return render(request, 'webpage/Scores/score_Arkanoid.html', {
        'pong': pong,
        'arkanoid': arkanoid,
        'space_invader': space_invader,

        'scores' : scores
    })

def scoreSpaceInvaders(request):
    pong = Games.objects.get(pk=1)
    arkanoid = Games.objects.get(pk=2)
    space_invader = Games.objects.get(pk=3)

    scores = Puntuaciones.objects.all().filter(gameFK=3).order_by('-puntuacion')

    return render(request, 'webpage/Scores/score_SpaceInvaders.html', {
        'pong': pong,
        'arkanoid': arkanoid,
        'space_invader': space_invader,

        'scores' : scores
    })

def arkanoid(request):
    return render(request, 'webpage/Unity/Arkanoid/index.html')

