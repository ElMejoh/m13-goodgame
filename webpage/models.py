from datetime import datetime

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class Games(models.Model):
    name = models.CharField(max_length=200, default="Name_Game")
    image = models.CharField(max_length=1000, default="Image_Url")
    def __str__(self):
        return self.name


class Puntuaciones(models.Model):
    gameFK = models.ForeignKey(Games, on_delete=models.CASCADE, default="")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    pub_date = models.DateTimeField('public date', default=datetime.now)
    puntuacion = models.IntegerField(default=0)
    def __str__(self):
        return "GAME: "+self.gameFK.name+" | USER: "+self.user.username+" | POINTS: "+self.puntuacion.__str__()+" | DATE: "+self.pub_date.__str__()

