from django.contrib import admin

# Register your models here.

from webpage.models import Puntuaciones, Games

admin.site.register(Puntuaciones)
admin.site.register(Games)