

########################################   APUNTES DE DJANGO (CARLES)   ########################################












########## Para crear Apps ##########

# [1] En Terminal
# $ python ./manage.py startapp <nombre_de_app>


# [2] Ir a <settings.py> del proyecto principal
# Ir a <INSTALLED_APPS = []> y añadir el nombre de la nueva app
#
# INSTALLED_APPS = [
#     'django.contrib.mombre_de_una_app',
#     'django.contrib.nombre_de_otra_app',
#     '<nombre_de_app>'                  <-------- $$ NUEVO $$
# ]


# [3] En Terminal
# $ python ./manage.py migrate
# Se creará una Base de Datos en Sqlite que se referenciará en <settings.py>
#
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': BASE_DIR / 'db.sqlite3',
#     }
# }












########## Empezar con la Views ##########

# [1] Ir al Archivo <views.py> de la nueva app
# definir la primera funcion:
#
#   def index(request):
#       return HttpResponse('Hola a todos!')
#
# Siempre le pasaremos request como parametro.


# [2] Ir al archivo (o si no está, crearlo) <urls.py> en la nueva app.
# y dentro habrá esto
#
################################
#   from django.urls import path
#   from . import views
#
#   urlpatterns = []
################################
#
# Dentro del urlpatterns, poner el path del nuevo view, tal que así:
#
#   urlpatterns = [
#       path('', views.index, name='index'),
#   ]
#
# Al no decirle nada en lo primero que pasas en el path '', nos mostrara el index por defecto


# [3] Ir a <urls.py> de la aplicacion principal (la original)
# y en su urlpatterns = [] añadir el path hacia la otra palicación:
#
########################################################################################
#   import <nombre_de_app>   <-------- $$ NUEVO $$
#
#   urlpatterns = [
#       path('admin/', admin.urls),
#       path('<nombre_de_app>/', include('<nombre_de_app>.urls'))  <-------- $$ NUEVO $$
#   ]
########################################################################################
#
# Y eso incluirá todas las URL que hay en la seguna app


# [4] Probemos si ha funcionado, haciendo run a la app
# y en la ruta / url del navegador añadir /<nombre_de_app>
# http://127.0.0.1:8000/<nombre_de_app>












########## Para manejar la Base de Datos ##########

# [1] Para manejar la BDD con Django, vamos a crear Clases
# Vamos a <models.py> de nuestra nueva aplicación
# Y allí podremos colocar las Clases que definirán nuestras tablas en la BDD
#
##########################################################################
# from django.db import models
#
# class Preguntas(models.Model):
#     pregunta = models.CharField(max_length=200)
#     pub_date = models.DateTimeField('date published')
#
# class Respuestas(models.Model):
#     preguntaFK = models.ForeignKey(Preguntas, on_delete=models.CASCADE)
#     respuesta = models.CharField(max_length=200)
#     votos = models.IntegerField(default=0)
##########################################################################
#
# Hemos puesto dos Clases / Tablas: Preguntas y Respuestas
#   * Hemos credo dos columnas para Preguntas:
#       - pregunta: CharField (Que sea un texto) y max_length=200 (Máximo 200 caracteres)
#       - pub_date: DateTimeField (Que sea una fecha y hora) y 'date published' (Su nombre)
#   * Hemos puesto tres columnas para Respuesta:
#       - preguntaFK: ForeignKey (Que sea una Foreing Key) y Preguntas, on_delete=models.CASCADE (Que sea de la tabla Preguntas, si borra la Pregunta, se borrarán las Respuestas)
#       - respuesta: CharField (Que sea un texto) y max_length=200 (Máximo 200 caracteres)
#       - votos: IntegerField (Que sea un integro) y default=0 (Que cuando se haga la respuesta el numero establecido sea 0)

# [2] Ahora hay que implementar los cambios en la BDD:
# Ir a Terminal:
# $ python .\manage.py makemigrations
# En la carpeta <migrations> ha aparecido una nueva migracion, por ejemplo: <0001_initial.py>

# [3] Migrar para confirmar los cambios
# Ir a Terminal:
# $ python .\manage.py migrate
# Y ahora aparecerán las nuevas tablas en la BDD












########## Para utilizar el Admin ##########

# [1] Crear primer el usuario Admin
# Ir a Terminal:
# $ python ./manage.py createsuperuser
# Pedirá name, email y password.


# [2] Iniciar con el nuevo user
# Ir a tu localhost/admin, ejemplo: 'http://127.0.0.1:8000/admin/'
# Y te aparecerá para inicarte.
# Inicia sesión con el usuario creado


# [3] Hacer aparecer las tablas en el Admin
# Ir a <admin.py> de la nueva aplicación y añadir las tablas
#
##########################################################
# from django.contrib import admin
#
# from WonderWeb.models import Preguntas, Respuestas
#
# admin.site.register(Preguntas)
# admin.site.register(Respuestas)
##########################################################


# [4] Mostrar más datos en las tablas admin
# Ir a <models.py>, donde hemos guardado nuestras Clases
# Añadir en cada clase un nuevo método: ir a < Code --> Override Methods --> __str__(self) >
# Y se te añadirá un nuevo método en la Clase:
#
#       def __str__(self):
#         return super().__str__()
#
# Es lo equivalente a un toString en Java
# Y puedes modificar lo que se mostrará:
#
###########################################################################
# class Preguntas(models.Model):
#     pregunta = models.CharField(max_length=200)
#     pub_date = models.DateTimeField('date published')
#     def __str__(self):
#         return self.pregunta
#
# class Respuestas(models.Model):
#     preguntaFK = models.ForeignKey(Preguntas, on_delete=models.CASCADE)
#     respuesta = models.CharField(max_length=200)
#     votos = models.IntegerField(default=0)
#     def __str__(self):
#         return self.respuesta
###########################################################################


# [5] Mostrar la información al cliente:
# Ir a <urls.py> de la nueva aplicación y allí crear los enlaces a las vistas que queremos hacer
#
##########################################################################
#   urlpatterns = [
#     path('', views.index, name='index'),
#     path('<int:question_id>/', views.details, name='details'),
#     path('<int:question_id>/results/', views.result, name='result'),
#     path('<int:question_id>/vote/', views.vote, name='vote'),
#   ]
##########################################################################
#
# Y trabajaremos a partir de ello












########## Empezar con las Vistas y Templates ##########

# [1] Crear la vista index (de prueba):
# Ir a <views.py> y cambiar la primera vista
#
##################################################
#   def index(request):
#     preguntas = Preguntas.objects.all()
#     respuesta = ''
#
#     for pregunta in preguntas:
#         respuesta += pregunta.pregunta + '<br>'
#
#     return HttpResponse(respuesta)
##################################################
#
# Nos mostrará todas las preguntas en el localhost/<nombre_de_app>


# [2] Para empezar a crear HTMLs primero hay que hacer lo siguiente
# Crear la carpeta <templates> en la nueva app
# Habrá que crearla para cada app nueva que hagamos, ya que se relacionará con la carpeta general de templates
# Dentro de la carpeta, habrá que crear su directorio personal
#
# - \<nombre_de_app>
#    - \templates           <-- $$ NUEVO $$
#       - \<nombre_de_app>  <-- $$ NUEVO $$
# - \<app_general>
# - \templates
#
# Y dentro de la carpeta dentro de template crearemos todos los templates referentes a su app.

# [3] Ir a <views.py> i en la <def index(request):> vamos a devolver un render.template
#
# def index(request):
#     return render(request, "<nombre_de_app>/templates/<nombre_de_app>/index.html")
#

# [4] Empezaremos por relacionar la informacion
# en el <views.py> y enviar un contexto, es decir, datos para que los use el html
#
# def index(request):
#     return render(request, "WonderWeb/index.html", {
#        "preguntas" : preguntas
#     })
#
# Y lo utilizaremos en el HTML <index.html> tal que así
#
############################################################################################
#
# {% if preguntas %}
#     <ul>
#         {% for pregunta in preguntas %}
#             <li><a href="/<nombre_de_app>/{{ pregunta.id }}/">{{ pregunta.pregunta }}</a></li>
#         {% endfor %}
#     </ul>
# {% else %}
#     <p>No hay preguntas</p>
# {% endif %}
#
############################################################################################

# [5] Crear un Details para cada prefunta
# Ir al <views.py> y crear una viex nueva para el details
#
# def details(request, pregunta_id):
#     return HttpResponse('Estás mirando la pregunta %s.' % pregunta_id)
#
# Para mostrar una respuesta sencilla al <a href=""> del <index.htm>
# Ir luega a <urls.py> y crear su url
#
# path('<int:pregunta_id>/', views.details, name='details')
#

# [6] Presonalizar el Details
# Vamos a crear el HTML <detail.html> y vamos a hacer que la view renerize el mismo
# Preo tambien le enviaremos una pregunta en concreto, con todos sus datos dentro
#
# def details(request, pregunta_id):
#     pregunta = Preguntas.objects.get(pk=pregunta_id)
#     return render(request, "WonderWeb/details.html", {
#         "pregunta" : pregunta
#     })
#
# Y en el <details.html> añadiremos lo siguiente:
#
################################################################
#
# <h1>{{ pregunta.pregunta }}</h1>
# <ul>
#     {% for respuesta in pregunta.respuestas_set.all %}
#         <li>{{ respuesta.respuesta }}</li>
#     {% endfor %}
# </ul>
#
################################################################
#
# Cogerá también a través de la Foreing Key de la tabla respuesta, todas sus respuestas
